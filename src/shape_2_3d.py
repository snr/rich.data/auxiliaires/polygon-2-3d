from pandas.core.frame import DataFrame as DFType
from pandas.core.series import Series as SType
import matplotlib.pyplot as plt
import geopandas as gpd
import trimesh as tm
import pandas as pd
import numpy as np
import typing as t
import os

from .utils.constants import IN, OUT


# *************************************************
# generate a 3D model from 3 shapefiles of the
# Palais-Royal describing the whole monument,
# the wings and the individual plots
# *************************************************


class ShapeTo3d:
    def __init__(self):
        self.gdf_f = gpd.read_file(os.path.join(IN, "Vasserot-PalaisRoyal-Empreinte.shp"))     # `gdf_f` = full geodataframe. full palais-royal
        self.gdf_p = gpd.read_file(os.path.join(IN, "Vasserot-PalaisRoyal-Parcelles.shp"))     # `gdf_w` = wings geodataframe. all 4 wings of the pr
        self.gdf_w = gpd.read_file(os.path.join(IN, "Vasserot-PalaisRoyal-Ailes.shp"))         # `gdf_p` = plot geodataframe. all the parcelles / plots
        self.pr = self.gdf_f.iloc[0]                                                           # geometry of the full Palais-Royal
        self.szfl = []                                                                         # `SiZe FuLl`.     numpy array of `[x,y,z]` dimensions for the whole Palais-Royal
        self.szpw = {}                                                                         # `SiZe Per Wing`. dict of `{ <wing name>: <numpy array of [x,y,z] dimensions for a wing of the palais royal> }`.
        self.szpp = {}                                                                         # `SiZe Per Plot`. dict of `{ <wing name>: <numpy array of [x,y,z] dimensions for a plot of a wing of the palais royal> }``
        self.scene = tm.Scene()                                                                # trimesh scene containing our 3D model
        return


    def locate_wings(self):
        """
        locate the 4 wings in `gdf_p` and `gdf_w`
        """
        # find which wing is represented by each feature in `gdf_w`
        prc = self.pr.geometry.centroid  # centroid of the Palais-Royal
        self.gdf_w["wing"] = ""
        self.gdf_w["color"] = ""
        self.gdf_w["_area"] = self.gdf_w.apply(lambda x:  x.geometry.area, axis=1)                       # area of the item
        self.gdf_w["_distance"] = self.gdf_w.apply(lambda x: x.geometry.centroid.distance(prc), axis=1)  # distance to center
        self.gdf_w["centroid_x"] = self.gdf_w.centroid.x
        self.gdf_w["centroid_y"] = self.gdf_w.centroid.y
        # first, find the palais royal garden
        self.gdf_w.loc[ self.gdf_w._distance.idxmin()  # smallest distance to the center => select the feature that's the most at the center => the garden
                        & self.gdf_w._area.idxmax()    # select the item with the largest area
                        , "wing" ] = "garden"
        # then, find the eastern, western, northern and southern wings
        self.gdf_w.loc[ self.gdf_w.centroid_x.idxmin(), "wing" ] = "west"
        self.gdf_w.loc[ self.gdf_w.centroid_x.idxmax(), "wing" ] = "east"
        self.gdf_w.loc[ self.gdf_w.centroid_y.idxmin(), "wing" ] = "south"
        self.gdf_w.loc[ self.gdf_w.centroid_y.idxmax(), "wing" ] = "north"

        # set the wing for all features in `self.gdf_p`
        def matchwing(s:SType) -> SType:
            mask = self.gdf_p.apply(lambda x: s.geometry.contains(x.geometry), axis=1)  # check if the wing `s` contains the fearture `x`
            self.gdf_p.loc[ mask, "wing" ] = s.wing
            return s
        self.gdf_p["wing"] = ""
        self.gdf_w.apply(matchwing, axis=1)

        # check that the wings were properly allocated
        self.gdf_w.plot(column=self.gdf_w.wing, legend=True, cmap="viridis")
        self.gdf_p.plot(column=self.gdf_p.wing, legend=True, cmap="spring")
        return self


    def order_wings(self):
        """
        order the plots inside `gdf_p`:
        * for the vertical wings, from south to north
        * for the horizontal wings, from west to east
        """
        def build_sorter(wing:str) -> SType:
            """
            create an ordered list of X or Y coordinates to use
            to determine the relative order of plots within a wing.
            :param wing : the wing name, one of `east`, `west`, `south`, `north`
            :returns    : a series containing the `x` or `y` coordinates in the current wing, ordered
            """
            assert wing in ["north", "south", "east", "west"], \
                   f"argument `wing` must be one of `['north', 'south', 'east', 'west']`, got `{wing}`"
            col = "centroid_x" if wing in ["north", "south"] else "centroid_y"
            return (self.gdf_p.loc[ self.gdf_p.wing.eq(wing), col ]
                              .sort_values()
                              .reset_index()
                              .drop(columns="index"))

        def order_by_sorter(wing:str, sorter:SType) -> None:
            """
            populate the column `gdf_p.order` by the relative position
            or plots in the current wing, using `sorter`
            :param wing   : the wing name: one of `east`, `west`, `south`, `north`
            :param sorter : the sorter created using `build_sorter()`
            """
            assert wing in ["north", "south", "east", "west"], \
                   f"argument `wing` must be one of `['north', 'south', 'east', 'west']`, got `{wing}`"
            col = "centroid_x" if wing in ["north", "south"] else "centroid_y"
            self.gdf_p.loc[ self.gdf_p.wing.eq(wing), "order" ] = (self.gdf_p.loc[ self.gdf_p.wing.eq(wing), col ]
                                                                             .apply(lambda x: sorter.loc[ sorter[col].eq(x) ]
                                                                                                    .index[0] ))  # position of `x` in `sorter`

        self.gdf_p["order"] = np.nan
        self.gdf_p["centroid_x"] = self.gdf_p.centroid.x
        self.gdf_p["centroid_y"] = self.gdf_p.centroid.y
        sorter = build_sorter("north")   # sorting key: west->east longitudes
        order_by_sorter("north", sorter)
        sorter = build_sorter("south")   # same
        order_by_sorter("south", sorter)
        sorter = build_sorter("east")    # sorting key: south->north longitudes
        order_by_sorter("east", sorter)
        sorter = build_sorter("west")    # same
        order_by_sorter("west", sorter)

        # check that all is good in the hood
        # for the `ax.annotate()`: https://stackoverflow.com/questions/71663911/how-to-add-labels-in-geopandas-geoplot
        fig, ax = plt.subplots()
        self.gdf_p.plot(ax=ax, legend=True, column=self.gdf_p.wing, cmap="spring")
        self.gdf_p.apply(lambda x: ax.annotate(text=str(x.order)
                                              , xy=x.geometry.centroid.coords[0]
                                              , ha="center")
                         , axis=1)
        return self

    def set_sizes(self):
        """
        define the size of each level in the 3d model:
        monument (`szfl`), wings and garden (`szpw`), plots (`szpp`).
        each dimension is represented as a numpy array of `x,y,z` dimensions.
        * x = the length, along the facade of a wing
        * y = the width, between the two facades of a wing
        * z = the height

        basic design decisions:
        * only the `x` is variable inside the model. `y` and `z` are always equal to 1
        * opposite wings have the same size: length of `N` == length of `S`,
          and length of `E`== length of `W`. however, the number of plots in
          `W` and `E` and in `S` and `N` vary.
        * for each `W/E` and `S/N` wing couple, the length of a single plot x = 1
          in the wing with the most plots. the x' plot length in the wing with the
          least plots is expressed relatively to x. for example, if `W` has 5 plots
          and `E` has 7, then the length of a single plot in `E` x=1 and the length
          of a single plot in `W` x'=7/5
        * in plot lengths, `x` >= 1


                                      _wing `W` is a large wing of size n
                                     |
                                     v               NW=(n,m)
                       ______________________________
                       |                            |
        wing `S` is    |                            |   wing `N` is
        a small wing ->|                            |<- a small wing
        of size m      |                            |   of size m
                       |                            |
                       ______________________________
                 SE=(0,0)            ^
                                     |
        ^ west/y axis                |_wing `E` is a
        |                              large wing of
        |                              size n
        +----> north/x axis
        """
        # ppw = plots_per_wing. structure: `{ <wing name>: <number of plots in the wing> }`
        ppw = { "west": self.gdf_p.wing.eq("west").sum(),
                "east": self.gdf_p.wing.eq("east").sum(),
                "north": self.gdf_p.wing.eq("north").sum(),
                "south": self.gdf_p.wing.eq("south").sum() }
        max_large_wing = "east" if ppw["east"] >= ppw["west"] else "west"      # name of large wing (east/west) with the biggest number of plots
        max_small_wing = "north" if ppw["north"] >= ppw["south"] else "south"  # name of small wing (north/south) with the biggest number of plots
        min_large_wing = "east" if ppw["east"] < ppw["west"] else "west"       # name of large wing (east/west) with the lowest number of plots
        min_small_wing = "north" if ppw["north"] < ppw["south"] else "south"   # name of small wing (north/south) with the lowest number of plots

        # determine the full length of the model = 20*8*3. `szfl` = SiZe FuLl
        self.szfl = np.array([ max(ppw["west"], ppw["east"]) + 2  # x: n° of plots in the larger wing with the most plots +2 angle plots
                             , max(ppw["south"], ppw["north"])    # y: n° of plots in the smaller wing with the most plots
                             , 1 ])                               # z: 1

        # determine the size of wings. `self.szpw` = SiZe Per Wing.
        # it's pretty easy:
        # * the `x` of each wing equals the number of plots
        #   in the long/small wing with the most plots
        # * the `y` and the `z` equals 1
        self.szpw = { "west": np.array([ppw[max_large_wing],1,1]),
                      "east": np.array([ppw[max_large_wing],1,1]),
                      "north": np.array([ppw[max_small_wing],1,1]),
                      "south": np.array([ppw[max_small_wing],1,1]),
                      "garden": np.array([ppw[max_large_wing]     # x = length of the longer wing
                                          , ppw[max_small_wing]-2   # y = length of the smaller wing - `y` size of the 2 wings around it
                                          , 1]) }

        # determine the length of individual plots. `self.szpp` = SiZe Per Plot
        # justification for the calculation of `x` sizes in the `min_*` wings:
        # given two wings `A` and `B` of the same size `o`.
        # `A` has `m` plots of size `k`, `B` has `n` plots of size `l`.
        # `B` has more plots than `A` (n<m) and `l=1` (the length of a plot in the wing with the most plots is 1)
        # we must determine the `k` size of a plot in `A` based on `l`.
        # m*k=o AND o=n*l
        # => k=o/m
        # => k=(n*l)/m
        # => <plot size in A> = ( <number of plots in B> * <plot size in B> ) / <number of plots in A> (same thing as above, but verbose)
        self.szpp = { "west": np.empty(shape=3),    # <wing name>: <[x,y,z] dimensions array for a single plot in the wing>
                      "east": np.empty(shape=3),
                      "north": np.empty(shape=3),
                      "south": np.empty(shape=3) }
        self.szpp[max_large_wing] = np.array([1,1,1])
        self.szpp[max_small_wing] = np.array([1,1,1])
        self.szpp[min_large_wing] = np.array([(self.szpp[max_large_wing][0]*ppw[max_large_wing]) / ppw[min_large_wing]
                                             ,1,1 ])
        self.szpp[min_small_wing] = np.array([(self.szpp[max_small_wing][0]*ppw[max_small_wing]) / ppw[min_small_wing]
                                             ,1,1 ])
        assert self.szpp[min_small_wing][0] * ppw[min_small_wing] == ppw[max_small_wing], \
               f"error in plot size calculation in `{min_small_wing}`"
        assert self.szpp[min_large_wing][0] * ppw[min_large_wing] == ppw[max_large_wing], \
               f"error in plot size calculation in `{min_large_wing}`"

        return self


    def rotate_small_wings(self):
        """
        rotate the small wings and plots inside the small wings.
        so far, they are represented with `x` being the "long" side, without
        taking into account their position relative to the model.
        in the final model, in the small wings, the `y` side will need
        to be the long side, so that the small wings are perpendicular
        to the long wings => invert `x` and `y`.
        """
        for k,v in self.szpp.items():
            self.szpp[k] = np.array([v[1],v[0],v[2]]) if k in ["north", "south"] else v
        for k,v in self.szpw.items():
            self.szpw[k] = np.array([v[1],v[0],v[2]]) if k in ["north", "south"] else v
        print("> szfl", self.szfl)
        print("> szpw", self.szpw)
        print("> szpp", self.szpp)

        return self


    def set_offset(self):
        """
        define an x,y,z offset for each level of the model: for the whole
        monument, for each wing and for each plot in each wing.
        * the offset is the position of an element's lower-left
          corner in the 3D-space of the whole model (south-east
          corner in geographical coordinates).
        * offsets are expressed as `np.array([x,y,z])`.
        * offset starts at `0,0,0` for the lower-left/southeast corner
          of the monument-level.
        * for each level, z+=1: the z-offset of the wing level is 1, and
          the z-offset of the plot level is 2.

        palais royal on the x/y and cardinal axis
        *****************************************
                 _________________________
                | |__________W__________| |
        y/west  | |                     | |
        ^       |S|          G          |N|
        |       | |_____________________| |
        |       |_|__________E__________|_|
        |
        +------>x/north
        """
        self.gdf_f["offset"] = [ np.array([0,0,0]) for _ in range(self.gdf_f.shape[0]) ]

        # 1) calculate the offset for `gdf_w`.
        # there are errors when filling an existing column with arrays,
        # so we need create the column and fill it with the arrays at
        # the same time. in turn, we split `gdf_w` by wing name, create
        # and populate `offset` column and finally recreate `gdf_w` from there.
        # d = "gdf_w"  # dataframe name
        gdf_w_s = self.gdf_w.loc[ self.gdf_w.wing.eq("south") ].copy()
        gdf_w_e = self.gdf_w.loc[ self.gdf_w.wing.eq("east") ].copy()
        gdf_w_n = self.gdf_w.loc[ self.gdf_w.wing.eq("north") ].copy()
        gdf_w_w = self.gdf_w.loc[ self.gdf_w.wing.eq("west") ].copy()
        gdf_w_g = self.gdf_w.loc[ self.gdf_w.wing.eq("garden") ].copy()

        # calculate the offset for each wing and append it to `gdf_w_*`
        w_s_offset = np.array([0,0,1])
        w_n_offset = np.array([ self.szpw["south"][0] + self.szpw["east"][0]  # x offset of south wing + x size of east wing
                              , w_s_offset[1]
                              , 1 ])
        w_e_offset = np.array([ self.szpw["south"][0]  # x size of the south wing
                              , w_s_offset[1]
                              , 1 ])
        w_w_offset = np.array([ self.szpw["south"][0]
                              , self.szpw["east"][1] + self.szpw["garden"][1]
                              , 1 ])
        w_g_offset = np.array([ self.szpw["south"][0]
                              , self.szpw["east"][1]
                              , 1 ])
        gdf_w_s["offset"] = gdf_w_s.apply(lambda x: w_s_offset, axis=1)
        gdf_w_n["offset"] = gdf_w_n.apply(lambda x: w_n_offset, axis=1)
        gdf_w_e["offset"] = gdf_w_e.apply(lambda x: w_e_offset, axis=1)
        gdf_w_w["offset"] = gdf_w_w.apply(lambda x: w_w_offset, axis=1)
        gdf_w_g["offset"] = gdf_w_g.apply(lambda x: w_g_offset, axis=1)

        # update gdf_w
        self.gdf_w = (pd.concat([gdf_w_s, gdf_w_n, gdf_w_e, gdf_w_w, gdf_w_g], axis=0)
                        .reset_index(drop=True))

        # 2) same thing with `gdf_p`
        gdf_p_s = self.gdf_p.loc[ self.gdf_p.wing.eq("south") ].copy()
        gdf_p_e = self.gdf_p.loc[ self.gdf_p.wing.eq("east") ].copy()
        gdf_p_n = self.gdf_p.loc[ self.gdf_p.wing.eq("north") ].copy()
        gdf_p_w = self.gdf_p.loc[ self.gdf_p.wing.eq("west") ].copy()

        # offset arrays are defined as lambda functions: the offset
        # depends on the `order` of the plot inside the wing. in all
        # functions, `x` = the order of the plot in the wing (type int)
        p_s_offset = np.array([0,0,2])  # only one plot in the south wing
        p_e_offset = lambda x: np.array([ self.szpw["south"][0] + x*self.szpp["east"][0]
                                        , p_s_offset[1]
                                        , 2 ])
        p_n_offset = lambda x: np.array([ self.szpw["south"][0] + self.szpw["east"][0]
                                        , x*self.szpp["north"][1]
                                        , 2 ])
        p_w_offset = lambda x: np.array([ self.szpw["south"][0] + x*self.szpp["west"][0]
                                        , self.szpw["east"][1] + self.szpw["garden"][1]
                                        , 2 ])
        gdf_p_s["offset"] = gdf_p_s.order.apply(lambda x: p_s_offset)
        gdf_p_e["offset"] = gdf_p_e.order.apply(lambda x: p_e_offset(x))
        gdf_p_n["offset"] = gdf_p_n.order.apply(lambda x: p_n_offset(x))
        gdf_p_w["offset"] = gdf_p_w.order.apply(lambda x: p_w_offset(x))

        # update gdf_p
        self.gdf_p = (pd.concat([gdf_p_s, gdf_p_e, gdf_p_n, gdf_p_w], axis=0)
                        .reset_index(drop=True))

        # check that all is good in the hood
        print(self.gdf_p[["wing", "order", "offset"]].sort_values(by=["wing", "order"], axis=0))
        print("\n".join( f"{k} : {v}" for k,v in self.szpp.items() ))
        return self


    def to3d(self):
        """
        we now have:
        * `szfl`, `szpw` and `szpp`, giving the
          [x,y,z] dimensions of the boxes of our 3D model
        * a column `offset` in our `gdf_p` and `gdf_w` dataframes
          giving an offset `[x,y,z]` at which to position each box in a 3D space
        from there, we create trimesh boxes and add them to a 3D scene to create
        a 3d model.

        our boxes are defined by bounds b = np.array([ A, G ]),
        where A and G are two points in space with coordinates
        A=[x,y,z] and G=[x',y',z'] as reprsented by the schema below.
        A corresponds to the offset, and G to the offset + the x,y,z
        dimensions of the box.
        (ps: it should also be possible to position our boxes in 3D
        space using translation matrixes based on our offset vectors,
        but there is an error in my matrix generation. see `translation()`
        at the end of this file.)


                 ^ Z
                 |
                 |E____________F
                /|            /|
               / |A__________/_|B_________> X
             H/__/__________/G /
              | /           | /
              |/____________|/
              /D            C
             /
            /
           v Y

        about translation matrixes (ended up being unused):
        * https://www.fil.univ-lille.fr/~aubert/m3d/m3d_transformation.pdf

        3D modelling libraries. we're using trimesh
        because it's the most actively developped
        * https://www.open3d.org/docs/latest/index.html
        * https://pypi.org/project/numpy-stl/
        * https://pymesh.readthedocs.io/en/latest/ (no longer supported?)
        * https://towardsdatascience.com/python-libraries-for-mesh-and-point-cloud-visualization-part-1-daa2af36de30
        """
        # the size of each box is defined by its bounds, between
        # its min(x,y,z) and max(x,y,z) coordinates (see `bounds2faces`).
        # they are defined using lambda functions, based on row `x` of `gdf_w`, `gdf_p`, `gdf_f`
        fbounds = lambda x: np.array([ x.offset, np.add(x.offset, self.szfl) ])          # full
        wbounds = lambda x: np.array([ x.offset, np.add(x.offset, self.szpw[x.wing]) ])  # wing
        pbounds = lambda x: np.array([ x.offset, np.add(x.offset, self.szpp[x.wing]) ])  # plot

        # add 3D boxes to our mesh array
        mesh = []  # array of 3d objects to add to the scene. structure: [ [ shape, level, wing, name ] ]
        for i,s in self.gdf_p.iterrows():
            mesh.append([ tm.primitives.Box( bounds=pbounds(s) )
                        , "plot"
                        , s.wing
                        , s.rich_uuid ])
        for i,s in self.gdf_w.iterrows():
            mesh.append([ tm.primitives.Box( bounds=wbounds(s) )
                        , "wing"
                        , s.wing
                        , s.rich_uuid ])
        for i,s in self.gdf_f.iterrows():
            mesh.append([ tm.primitives.Box( bounds=fbounds(s) )
                        , "full"
                        , None
                        , s.rich_uuid ])

        # add color to our meshes + add meshes to the scene
        for m in mesh:
            m[0].visual.face_colors = ([255, 195, 0, 255] if m[1]=="plot"   # pink
                                       else [255, 87, 51] if m[1]=="wing"  # green
                                       else [199, 0, 57])                   # maroon
            self.scene.add_geometry(m[0]
                                   , geom_name=m[3]
                                   , metadata={ "level": m[1], "wing": m[2] })

        # save as gltf and display
        self.scene.export(os.path.join(OUT, "palais-royal.gltf"), file_type="gltf")

        self.scene.show()
        self.scene.explode(2)
        self.scene.show()

        return self


    def pipeline(self):
        # reproject if necessary
        for k,v in { "gdf_f": self.gdf_f, "gdf_p": self.gdf_p, "gdf_w": self.gdf_w }.items():
            if v.crs != "EPSG:3949":
                print(f"`{k}` in CRS `v.crs`, reprojecting it to `EPSG:3949`")
                v.to_crs("EPSG:3949")
                setattr(self, k, v)

        # only keep plots in `gdf_p` that are in the Palais-Royal (and not in adjacent streets)
        self.gdf_p = self.gdf_p.loc[ self.gdf_p.apply(lambda x: self.pr.geometry.contains(x.geometry), axis=1) ]  # keep only the plots that intersect with `pr.geometry`

        (self.locate_wings()
             .order_wings()
             .set_sizes()
             .rotate_small_wings()
             .set_offset()
             .to3d()
        )
        plt.show()
        return


# UNUSED FUNCTIONS

#             def translation(v:t.List[str]) -> np.ndarray:
#             """
#             **************** DOESN'T WORK, UNUSED !! ****************

#             the translation matrix moves an object in 3D space based on
#             the vector `v`, which in our case is an XYZ offset calculated
#             in `set_offset()`. the vector is from `[0,0,0]` to where our
#             object should be.

#             given a a point M(x,y,z) and a destination point M'(x',y',z'),
#             the translation vector is v(vz,vy,vz) such that:
#             * vx = x'-x
#             * vy = y'-y
#             the translation matrix T corresponding to the coordiates vector
#             v=[vx, vy, vz] is:
#                 1 0 0 vx
#             T = 0 1 0 vy
#                 0 0 1 vz
#                 0 0 0 1
#             see: https://www.fil.univ-lille.fr/~aubert/m3d/m3d_transformation.pdf
#                  https://fr.wikipedia.org/wiki/Translation (partie "Coordonnées cartésiennes)
#             """
#             og = [0., 0., 0.]
#             og_ = [0., 0., 0., 1]
#             a = np.array([ [1,   0,   0,   0]
#                          , [0,   1,   0,   0]
#                          , [0,   0,   1,   0]
#                          , [v[0],v[1],v[2],1] ])
#             b = np.array([ [ 1,0,0,v[0] ]
#                          , [ 0,1,0,v[1] ]
#                          , [ 0,0,1,v[2] ]
#                          , [ 0,0,0,1    ] ])
#             # based on the debugs below, the good matrix is `a`
#             # but the `tm.primitives.Box.transform` property outputs a matrix
#             # like `b↓ so there's probably an error in the values of the `v` vector
#             print(f"* og    {og}\n"
#                   f"* v     {v}\n"
#                   f"* add   {np.add(og, v)}\n"
#                   f"* mul a {np.matmul(og_, a)}\n"
#                   f"* mul b {np.matmul(og_, b)}\n"
#                   f"*****************************")
#             return a

#         def bounds2faces(bnd:np.ndarray) -> np.ndarray:
#             """
#             generate an array of 6 quad faces representing a box
#             defined by the bounds `bnd`.
#             b = np.array([ [x,y,z], [x',y',z'] ]), with:
#             * [x,y,z] the lower left point = `A` in the schema below
#               (the point of the box with min x,y,z values)
#             * [x',y',z'] the upper right point = `G` in the schema below
#               (the point of the box with max x,y,z values).
#             in our case, A = [x,y,z] = the offset,
#                          G = [x',y',z'] = the offset + the shape of the box

#                  ^ Z
#                  |
#                  |E____________F
#                 /|            /|
#                / |A__________/_|B_________> X
#              H/__/__________/G /
#               | /           | /
#               |/____________|/
#               /D            C
#              /
#             /
#            v Y

#             :param bnd: the bounds, defined as np.array of shape (2,3)
#             :returns  : an np array of shape (6,4)
#             """
#             a = np.array([ bnd[0][0], bnd[0][1], bnd[0][2] ])  # xyz = bnd[0]
#             b = np.array([ bnd[1][0], bnd[0][1], bnd[0][2] ])  # x'yz
#             c = np.array([ bnd[1][0], bnd[1][1], bnd[0][2] ])  # x'y'z
#             d = np.array([ bnd[0][0], bnd[1][1], bnd[0][2] ])  # xy'z
#             e = np.array([ bnd[0][0], bnd[0][1], bnd[1][2] ])  # xyz'
#             f = np.array([ bnd[1][0], bnd[0][1], bnd[1][2] ])  # x'yz'
#             g = np.array([ bnd[1][0], bnd[1][1], bnd[1][2] ])  # x'y'z' = bnd[1]
#             h = np.array([ bnd[0][1], bnd[1][1], bnd[1][2] ])  # xy'z'

#             return np.array([ [a,b,c,d]
#                             , [e,f,g,h]
#                             , [a,d,h,e]
#                             , [a,b,f,e]
#                             , [d,c,g,h]
#                             , [c,b,f,g] ])
