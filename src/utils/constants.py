import os

UTILS = os.path.abspath(os.path.dirname(__file__))
SRC = os.path.abspath(os.path.join(UTILS, os.pardir))
ROOT = os.path.abspath(os.path.join(SRC, os.pardir))
IN = os.path.abspath(os.path.join(ROOT, "in"))
OUT = os.path.abspath(os.path.join(ROOT, "out"))

