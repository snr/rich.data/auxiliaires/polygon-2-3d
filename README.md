# PRODUCTION D'UN MODÈLE 3D DU PALAIS-ROYAL À PARTIR DE *SHAPEFILES*

Ce dépôt permet la génération d'un modèle 3D schématique du Palais-Royal à partir de fichiers contenant
un découpage vectoriel de celui-ci, réalisé dans un SIG à partir de fonds de cartes historiques. Les fichiers
vectoriels sont produits à partir du découpage parcellaire de Paris réalisé par le [projet Alpage](https://alpage.huma-num.fr/) 
à partir du plan Vasserot.

---

## Installation et utilisation

L'installation fonctionne pour Linux et MacOs et demande quelques petites corrections pour Windows.

```bash
# installation
git clone https://gitlab.inha.fr/snr/rich.data/auxiliaires/polygon-2-3d.git  # cloner le dépôt
cd polygon-2-3d                                                              # se déplacer dans le dossier
python3 -m venv env                                                          # créer l'environnement python
source env/bin/activate                                                      # le sourcer
pip install -r requirements.txt                                              # installer les dépendances
python main.py                                                               # lancer le projet
```

---

## Structure du dépôt

```
racine/
  |__in/ : fichiers vectoriels utilisés en entrée
  |__src/: scripts python de transformation
  |__out/: fichiers GLTF produits en sortie
```

---

## Licence

Le code et le modèle 3D produit par celui-ci au format GLTF sont disponibles sous licence GNU-GPL.
Les données vectorielles utilisées en entrée sont disponibles sous licence ODBL.


